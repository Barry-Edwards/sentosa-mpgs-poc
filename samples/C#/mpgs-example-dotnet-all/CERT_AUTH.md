## Authenticating to the Gateway using a certficate
1. Download the test certificate from the merchant admin portal. Your merchant must be configured to use SSL certificate authentication.

2. Convert the test.crt and test.key files included in the download to PKCS12 format using the [OpenSSL tool](https://www.openssl.org/source/).

        openssl pkcs12 -export -out certificate.p12 -inkey test.key -in test.crt

3. Import the certificate using Microsoft Wizard Tool and it must be available for all machine and the current SO user


4. Pass the keystore information and the certificate as environment variables.

NOTE: The following parameters are required for certificate authentication: keystore path, keystore password, merchant ID, gateway base URL, currency, and gateway certificate host URL.

#### Environment variables:
1. Set the environment variables
    - On Mac/Linux: Use the ```export``` command:

            prompt> export GATEWAY_MERCHANT_ID=YOUR_MERCHANT_ID 
            prompt> export GATEWAY_BASE_URL=YOUR_GATEWAY_BASE_URL
            prompt> export GATEWAY_CERT_HOST_URL=YOUR_GATEWAY_CERT_HOST_URL
            prompt> export KEYSTORE_PASSWORD=YOUR_KEYSTORE_PASSWORD (the keystore password is the same as the test merchant ID, e.g. TESTMERCHANTID)
            prompt> export KEYSTORE_PATH=PATH_TO_PKCS12
            prompt> export GATEWAY_CURRENCY=YOUR_CURRENCY (optional - default is USD)
            prompt> export GATEWAY_VERSION=YOUR_VERSION (optional - default is version 45)

    - On Windows, use the ```set``` command:

            prompt> set GATEWAY_MERCHANT_ID=YOUR_MERCHANT_ID
            prompt> set GATEWAY_BASE_URL=YOUR_GATEWAY_BASE_URL
            prompt> set GATEWAY_CERT_HOST_URL=YOUR_GATEWAY_CERT_HOST_URL
            prompt> set KEYSTORE_PASSWORD=YOUR_KEYSTORE_PASSWORD (the keystore password is the same as the test merchant ID, e.g. TESTMERCHANTID)
            prompt> set KEYSTORE_PATH=PATH_TO_PKCS12
            prompt> set GATEWAY_CURRENCY=YOUR_CURRENCY (optional - default is USD)
            prompt> set GATEWAY_VERSION=YOUR_VERSION (optional - default is version 45)


2. Run the following:

dotnet build
dotnet run 

3. Navigate to *http://localhost:5000* to test locally
u
## Running Hosted Checkout locally
In order to test Hosted Checkout locally, you'll need to use a tunneling tool like [ngrok](https://ngrok.com/). Run the command ```./ngrok http 5000``` and navigate to the https tunnel referenced in the output.

## Disclaimer
This software is intended for **TEST/REFERENCE** purposes **ONLY** and is not intended to be used in a production environment.


4.MacOS information

## Using environments variables in applications started by Visual Studio
By default App launch by finder don't inherance the system variables. To using the system variables information, Visual Studio must be launched by terminal:

/Applications/Visual\ Studio.app/Contents/MacOS/VisualStudio &


##SSL on MacOS

Keychain and SSL certificates isn't supported so far on dotnet core 2.0 for Mac OS Sierra High (Libcurl issue : https://github.com/dotnet/corefx/issues/9728).

