using System;
using gateway_csharp_sample_code.Controllers;
using gateway_csharp_sample_code.Gateway;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using Xunit;

namespace gateway_csharp_sample_code.xunittest
{

    /// <summary>
    /// Test base class
    /// </summary>
    public class BaseTest
    {

        public BaseTest()
        {

        }

        /// <summary>
        /// Mock controller context.
        /// </summary>
        /// <returns>The controller context.</returns>
        protected ControllerContext getControllerContext(){
            var request = new Mock<HttpRequest>();


            request.Setup(x => x.Scheme).Returns("Scheme");
            request.Setup(x => x.Host).Returns(new HostString());
            request.Setup(x => x.PathBase).Returns("/Path");

            var form = new Mock<IFormCollection>();
            request.Setup(x => x.Form).Returns(form.Object);

            var context = new Mock<HttpContext>();
            context.SetupGet(x => x.Request).Returns(request.Object);

            var controllerContext = new ControllerContext();
            controllerContext.HttpContext = context.Object;
            controllerContext.RouteData = new RouteData();


            return controllerContext;
        }

        /// <summary>
        /// Checks the page expected against page returned into action result.
        /// </summary>
        /// <returns><c>true</c>, if page against action result was checked, <c>false</c> otherwise.</returns>
        /// <param name="page">Page.</param>
        /// <param name="result">Result.</param>
        protected Boolean checkPageAgainstActionResult(string page, IActionResult result){
            bool isCorrect;
            if (result is RedirectResult) {
                isCorrect =  ((Microsoft.AspNetCore.Mvc.RedirectResult)result).Url == page;
            } else if (result is  ViewResult) {
                isCorrect =  ((Microsoft.AspNetCore.Mvc.ViewResult)result).ViewName == page;
            } else {
                isCorrect = false;
            }
            return isCorrect;
        }

    }
}
