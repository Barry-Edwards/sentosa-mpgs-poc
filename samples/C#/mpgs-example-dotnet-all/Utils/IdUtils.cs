using System;

namespace gateway_csharp_sample_code.Utils
{
    public class IdUtils
    {
        public static string generateSampleId()
        {
            return Guid.NewGuid().ToString().Replace("-", string.Empty).Substring(0, 10);
        }
    }
}
